<?php
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/kuliners', 'KulinerController@index')
        ->name('kuliner.index');
    Route::get('/kuliners/create', 'KulinerController@create');
    Route::post('/kuliners', 'KulinerController@store')
        ->name('kuliner.create');
    Route::get('/kuliners/{kuliner}/edit', 'KulinerController@edit');
    Route::patch('/kuliners/{kuliner}', 'KulinerController@update')
        ->name('kuliner.update');
    Route::delete('/kuliners/{kuliner}', 'KulinerController@destroy')
        ->name('kuliner.delete');

    Route::get('/wisatas', 'WisatasController@index')
        ->name('wisata.index');
    Route::get('/wisatas/create', 'WisatasController@create');
    Route::post('/wisatas', 'WisatasController@store')
        ->name('wisata.create');
    Route::get('/wisatas/{wisata}/edit', 'WisatasController@edit');
    Route::patch('/wisatas/{wisata}', 'WisatasController@update')
        ->name('wisata.update');
    Route::delete('/wisatas/{wisata}', 'WisatasController@destroy')
        ->name('wisata.delete');

    Route::get('/lokers', 'LokersController@index')
        ->name('loker.index');
    Route::get('/lokers/create', 'LokersController@create');
    Route::post('/lokers', 'LokersController@store')
        ->name('loker.create');
    Route::get('/lokers/{loker}/edit', 'LokersController@edit');
    Route::patch('/lokers/{loker}', 'LokersController@update')
        ->name('loker.update');
    Route::delete('/lokers/{loker}', 'LokersController@destroy')
        ->name('loker.delete');

    Route::get('/events', 'EventsController@index')
        ->name('event.index');
    Route::get('/events/create', 'EventsController@create');
    Route::post('/events', 'EventsController@store')
        ->name('event.create');
    Route::get('/events/{event}/edit', 'EventsController@edit');
    Route::patch('/events/{event}', 'EventsController@update')
        ->name('event.update');
    Route::delete('/events/{event}', 'EventsController@destroy')
        ->name('event.delete');

    Route::get('/oleholehs', 'OlehOlehsController@index')
        ->name('oleholeh.index');
    Route::get('/oleholehs/create', 'OlehOlehsController@create');
    Route::post('/oleholehs', 'OlehOlehsController@store')
        ->name('oleholeh.create');
    Route::get('/oleholehs/{oleholeh}/edit', 'OlehOlehsController@edit');
    Route::patch('/oleholehs/{oleholeh}', 'OlehOlehsController@update')
        ->name('oleholeh.update');
    Route::delete('/oleholehs/{oleholeh}', 'OlehOlehsController@destroy')
        ->name('oleholeh.delete');

    Route::get('/merchs', 'MerchsController@index')
        ->name('merch.index');
    Route::get('/merchs/create', 'MerchsController@create');
    Route::post('/merchs', 'MerchsController@store')
        ->name('merch.create');
    Route::get('/merchs/{merch}/edit', 'MerchsController@edit');
    Route::patch('/merchs/{merch}', 'MerchsController@update')
        ->name('merch.update');
    Route::delete('/merchs/{merch}', 'MerchsController@destroy')
        ->name('merch.delete');

    Route::get('/penginapans', 'PenginapansController@index')
        ->name('penginapan.index');
    Route::get('/penginapans/create', 'PenginapansController@create');
    Route::post('/penginapans', 'PenginapansController@store')
        ->name('penginapan.create');
    Route::get('/penginapans/{penginapan}/edit', 'PenginapansController@edit');
    Route::patch('/penginapans/{penginapan}', 'PenginapansController@update')
        ->name('penginapan.update');
    Route::delete('/penginapans/{penginapan}', 'PenginapansController@destroy')
        ->name('penginapan.delete');

    Route::get('/charities', 'CharitiesController@index')
        ->name('charity.index');
    Route::get('/charities/create', 'CharitiesController@create');
    Route::post('/charities', 'CharitiesController@store')
        ->name('charity.create');
    Route::get('/charities/{charity}/edit', 'CharitiesController@edit');
    Route::patch('/charities/{charity}', 'CharitiesController@update')
        ->name('charity.update');
    Route::delete('/charities/{charity}', 'CharitiesController@destroy')
        ->name('charity.delete');

    Route::get('/users', 'UsersController@index')
        ->name('user.index');
    Route::post('/users', 'UsersController@store')
        ->name('user.create');
    Route::get('/users/create', 'UsersController@create');
    Route::get('/users/{user}/edit', 'UsersController@edit');
    Route::patch('/users/{user}', 'UsersController@update')
        ->name('user.update');
    Route::post('/activation/users/{user}', 'UsersStatusController@store')
        ->name('user.active');
    Route::delete('/activation/users/{user}', 'UsersStatusController@destroy')
        ->name('user.inactive');

    Route::post('/publishing/kuliners/{kuliner}', 'PublishingKulinerController@store')
        ->name('kuliner.publish');
    Route::delete('/publishing/kuliners/{kuliner}', 'PublishingKulinerController@destroy')
        ->name('kuliner.unpublish');

    Route::post('/publishing/events/{event}', 'PublishingEventController@store')
        ->name('event.publish');
    Route::delete('/publishing/events/{event}', 'PublishingEventController@destroy')
        ->name('event.unpublish');

    Route::post('/publishing/oleholehs/{oleholeh}', 'PublishingOlehOlehController@store')
        ->name('oleholeh.publish');
    Route::delete('/publishing/oleholehs/{oleholeh}', 'PublishingOlehOlehController@destroy')
        ->name('oleholeh.unpublish');

    Route::post('/publishing/wisatas/{wisata}', 'PublishingWisataController@store')
        ->name('wisata.publish');
    Route::delete('/publishing/wisatas/{wisata}', 'PublishingWisataController@destroy')
        ->name('wisata.unpublish');

    Route::post('/publishing/lokers/{loker}', 'PublishingLokerController@store')
        ->name('loker.publish');
    Route::delete('/publishing/lokers/{loker}', 'PublishingLokerController@destroy')
        ->name('loker.unpublish');

    Route::post('/publishing/penginapans/{penginapan}', 'PublishingPenginapanController@store')
        ->name('penginapan.publish');
    Route::delete('/publishing/penginapans/{penginapan}', 'PublishingPenginapanController@destroy')
        ->name('penginapan.unpublish');

    Route::post('/publishing/charities/{charity}', 'PublishingCharityController@store')
        ->name('charity.publish');
    Route::delete('/publishing/charities/{charity}', 'PublishingCharityController@destroy')
        ->name('charity.unpublish');


    Route::get('/images', 'ImagesController@index');
    Route::get('/medias', 'ImagesController@index');
    Route::post('/medias', 'ImagesController@store')
        ->name('media.store')
        ->middleware('optimizeImages');
    Route::patch('/medias/{image}', 'ImagesController@update')
        ->name('media.update');
    Route::delete('/medias/{image}', 'ImagesController@destroy')
        ->name('media.delete');

});