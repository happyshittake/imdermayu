<?php
namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use Sluggable;

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() : array
    {
        return [
            'name' => [
                'source' => 'original_name'
            ]
        ];
    }
}
