<?php
namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Http\QueryFilters\Traits\FiltersRecords;
use App\Content;

class Kuliner extends Model
{
    use Sluggable, Searchable, FiltersRecords, Content;

    protected $guarded = [];

    protected $hidden = [
        'searchable'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];

    public function toArray()
    {
        $author = $this->author;
        $location = $this->location;

        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'price' => $this->price,
            'address' => $this->address,
            'image' => $this->image,
            'open_time' => $this->open_time,
            'close_time' => $this->close_time,
            'published_at' => optional($this->published_at)->format('Y-m-d H:i:s'),
            'published' => $this->published,
            'author' => [
                'id' => \optional($author)->id,
                'name' => \optional($author)->name
            ],
            'location' => [
                'lat' => \optional(optional($location)->point)->getLat(),
                'lon' => \optional(optional($location)->point)->getLng()
            ]
        ];
    }

    public function getOpenTimeAttribute($value)
    {
        if (!$value) {
            return null;
        }

        return Carbon::createFromFormat('H:i:s', $value)->format('H:i');
    }

    public function getCloseTimeAttribute($value)
    {
        if (!$value) {
            return null;
        }

        return Carbon::createFromFormat('H:i:s', $value)->format('H:i');
    }

    public function setOpenTimeAttribute($value)
    {
        $this->attributes['open_time'] = Carbon::createFromFormat("H:i", $value)->format('H:i:s');
    }

    public function setCloseTimeAttribute($value)
    {
        $this->attributes['close_time'] = Carbon::createFromFormat("H:i", $value)->format("H:i:s");
    }

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'address' => $this->address
        ];
    }

    public function searchableOptions()
    {
        return [
            'rank' => [
                'fields' => [
                    'title' => 'A',
                    'body' => 'B',
                    'address' => 'C'
                ]
            ]
        ];
    }
}
