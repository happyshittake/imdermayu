<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Laravel\Scout\Searchable;
use App\Http\QueryFilters\Traits\FiltersRecords;
use App\Content;

class Loker extends Model
{
    use Sluggable, Searchable, FiltersRecords, Content;

    protected $guarded = [];

    protected $hidden = [
        'searchable'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'address' => $this->address
        ];
    }

    public function searchableOptions()
    {
        return [
            'rank' => [
                'fields' => [
                    'title' => 'A',
                    'body' => 'B',
                    'address' => 'C'
                ]
            ]
        ];
    }

    public function toArray()
    {
        $author = $this->author;
        $location = $this->location;

        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'image' => $this->image,
            'published_at' => optional($this->published_at)->format('Y-m-d H:i:s'),
            'published' => $this->published,
            'author' => [
                'id' => \optional($author)->id,
                'name' => \optional($author)->name
            ],
            'location' => [
                'lat' => \optional(optional($location)->point)->getLat(),
                'lon' => \optional(optional($location)->point)->getLng()
            ]
        ];
    }

}
