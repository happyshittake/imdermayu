<?php
namespace App;

use App\Location;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;


trait Content
{
    public function location()
    {
        return $this->morphOne(Location::class, 'location');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getPublishedAttribute()
    {
        return $this->published_at && $this->published_at->lte(Carbon::now());
    }


    public function scopePublished(Builder $query)
    {
        return $query->whereNotNull('published_at')->where('published_at', '<=', Carbon::now());
    }
}