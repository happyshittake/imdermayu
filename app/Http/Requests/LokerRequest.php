<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LokerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'body' => 'required|string',
            'image' => 'required|url',
            'published' => 'boolean',
            'published_at' => 'required_if:published,true|date_format:Y-m-d H:i:s',
            'lat' => 'required|numeric',
            'lon' => 'required|numeric'
        ];
    }
}
