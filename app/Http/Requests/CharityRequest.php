<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'body' => 'required|string',
            'image' => 'required|url',
            'published' => 'boolean',
            'published_at' => 'required_if:published,true|date_format:Y-m-d H:i:s',
            'address' => 'required|string',
            'bank_name' => 'required|string',
            'bank_account_no' => 'required|string',
            'bank_account_owner' => 'required|string',
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
            'contact' => 'required|string',
        ];
    }
}
