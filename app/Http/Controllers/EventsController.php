<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Http\QueryFilters\EventQueryFilter;
use App\Http\Resources\EventResource;
use App\Http\Requests\EventRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class EventsController extends Controller
{
    public function index(EventQueryFilter $filters)
    {
        $q = Event::query();

        $events = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $events->load('location', 'author');

        $data = EventResource::collection($events);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('event.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Event $event)
    {
        $event->delete();

        if (request()->expectsJson()) {
            return EventResource::make($event);
        }

        return \back();
    }

    public function store(EventRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $event = new Event(\array_except($data, ['lat', 'lon', 'published']));
        $event = $request->user()->events()->save($event);
        $event->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return EventResource::make($event)
                ->additional(['redirect' => \route('event.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Event $event, EventRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $event->update(array_except($data, ['lat', 'lon', 'published']));
        if ($event->location) {
            $event->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $event->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return EventResource::make($event)
                ->additional(['redirect' => \route('event.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $event = new Event();

        return \view('event.create', \compact('event'));
    }

    public function edit(Event $event)
    {
        return \view('event.edit', \compact('event'));
    }
}
