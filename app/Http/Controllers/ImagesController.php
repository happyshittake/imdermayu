<?php
namespace App\Http\Controllers;


use App\Http\Resources\MediaResource;
use App\Image;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;

class ImagesController extends Controller
{
    public function store()
    {
        $file = request()->file('file');
        $originalName = $file->getClientOriginalName();
        $sluggedName = SlugService::createSlug(
            Image::class,
            'name',
            pathinfo($originalName, PATHINFO_FILENAME)
        );

        $image = Image::create([
            'original_name' => $file->getClientOriginalName(),
            'name' => $sluggedName,
            'ext' => $file->guessClientExtension()
        ]);

        $file->storeAs('.', "{$sluggedName}.{$file->guessClientExtension()}");

        if (request()->expectsJson()) {
            return MediaResource::make($image)->response()->setStatusCode(201);
        }

        return back();
    }

    public function update(Image $image)
    {
        $data = request()->validate([
            'caption' => 'required|string'
        ]);

        $image->caption = $data['caption'];
        $image->save();

        if (request()->expectsJson()) {
            return MediaResource::make($image);
        }

        return back();
    }

    public function index()
    {
        $images = Image::orderBy('id', 'DESC')->paginate(16);

        if (request()->expectsJson()) {
            return MediaResource::collection($images);
        }

        return view('image.index', ['data' => MediaResource::collection($images)->resource->toArray()]);
    }

    public function destroy(Image $image)
    {
        \Storage::delete("{$image->name}.{$image->ext}");

        $image->delete();

        if (request()->expectsJson()) {
            return MediaResource::make($image);
        }

        return back();
    }
}