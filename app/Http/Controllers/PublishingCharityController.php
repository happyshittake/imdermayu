<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charity;
use App\Http\Resources\CharityResource;

class PublishingCharityController extends Controller
{
    public function store(Charity $charity)
    {
        $charity->published_at = Carbon::now();
        $charity->save();

        if (\request()->expectsJson()) {
            return CharityResource::make($charity)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Charity $charity)
    {
        $charity->published_at = null;
        $charity->save();

        if (\request()->expectsJson()) {
            return CharityResource::make($charity);
        }

        return \back();
    }
}
