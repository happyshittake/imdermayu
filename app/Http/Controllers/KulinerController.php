<?php
namespace App\Http\Controllers;


use App\Kuliner;
use App\Http\Resources\KulinerResource;
use App\Http\QueryFilters\KulinerQueryFilter;
use Carbon\Carbon;
use App\Http\Requests\KulinerRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class KulinerController extends Controller
{
    public function index(KulinerQueryFilter $filters)
    {
        $q = Kuliner::query();

        $kuliners = $q->filterBy($filters)->orderBy('published_at', 'ASC')->orderBy('id', 'DESC')->paginate(12);

        $kuliners->load('location', 'author');

        $data = KulinerResource::collection($kuliners);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('kuliner.index', ['data' => $data->resource->toArray()]);
    }

    public function store(KulinerRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $kuliner = new Kuliner(\array_except($data, ['lat', 'lon', 'published']));
        $kuliner = $request->user()->kuliners()->save($kuliner);
        $kuliner->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return KulinerResource::make($kuliner)
                ->additional(['redirect' => \route('kuliner.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Kuliner $kuliner, KulinerRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $kuliner->update(array_except($data, ['lat', 'lon', 'published']));
        if ($kuliner->location) {
            $kuliner->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $kuliner->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return KulinerResource::make($kuliner)
                ->additional(['redirect' => \route('kuliner.index')])
                ->response();
        }

        return \back();
    }

    public function edit(Kuliner $kuliner)
    {
        return view('kuliner.edit', compact('kuliner'));
    }

    public function create()
    {
        $kuliner = new Kuliner();

        return \view('kuliner.create', \compact('kuliner'));
    }

    public function destroy(Kuliner $kuliner)
    {
        $kuliner->delete();

        if (request()->expectsJson()) {
            return KulinerResource::make($kuliner);
        }

        return \back();
    }
}