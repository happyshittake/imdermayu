<?php
namespace App\Http\Controllers;


use App\Kuliner;
use Carbon\Carbon;
use App\Http\Resources\KulinerResource;
use App\Http\Controllers\Controller;


class PublishingKulinerController extends Controller
{
    public function store(Kuliner $kuliner)
    {
        $kuliner->published_at = Carbon::now();
        $kuliner->save();

        if (\request()->expectsJson()) {
            return KulinerResource::make($kuliner)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Kuliner $kuliner)
    {
        $kuliner->published_at = null;
        $kuliner->save();

        if (\request()->expectsJson()) {
            return KulinerResource::make($kuliner);
        }

        return \back();
    }
}