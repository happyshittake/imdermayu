<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\UserQueryFilter;
use App\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\CreateUserRequest;

class UsersController extends Controller
{
    public function index(UserQueryFilter $filters)
    {
        $users = User::filterBy($filters)->paginate(12);

        $data = UserResource::collection($users);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('user.index', ['data' => $data->resource->toArray()]);
    }

    public function store(CreateUserRequest $request)
    {
        $data = $request->validated();

        $data['is_active'] = true;

        $user = User::create($data);

        if (request()->expectsJson()) {
            return UserResource::make($user)
                ->additional(['redirect' => \route('user.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(User $user)
    {
        $data = \request()->validate(
            [
                'name' => 'required|string',
                'password' => 'string|nullable'
            ]
        );

        $user->update($data);

        if (request()->expectsJson()) {
            return UserResource::make($user)
                ->additional(['redirect' => \route('user.index')]);
        }

        return \back();
    }

    public function create()
    {
        $user = new User;

        return view('user.create', \compact('user'));
    }

    public function edit(User $user)
    {
        return view('user.edit', \compact('user'));
    }
}
