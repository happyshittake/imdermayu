<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Resources\LokerResource;
use App\Loker;

class PublishingLokerController extends Controller
{
    public function store(Loker $loker)
    {
        $loker->published_at = Carbon::now();
        $loker->save();

        if (\request()->expectsJson()) {
            return LokerResource::make($loker)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Loker $loker)
    {
        $loker->published_at = null;
        $loker->save();

        if (\request()->expectsJson()) {
            return LokerResource::make($loker);
        }

        return \back();
    }
}
