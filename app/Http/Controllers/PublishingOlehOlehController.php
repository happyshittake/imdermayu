<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OlehOleh;
use App\Http\Resources\OlehOlehResource;

class PublishingOlehOlehController extends Controller
{
    public function store(OlehOleh $oleholeh)
    {
        $oleholeh->published_at = Carbon::now();
        $oleholeh->save();

        if (\request()->expectsJson()) {
            return OlehOlehResource::make($oleholeh)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(OlehOleh $oleholeh)
    {
        $oleholeh->published_at = null;
        $oleholeh->save();

        if (\request()->expectsJson()) {
            return OlehOlehResource::make($oleholeh);
        }

        return \back();
    }
}
