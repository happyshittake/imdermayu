<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\WisataQueryFilter;
use App\Wisata;
use App\Http\Resources\WisataResource;
use App\Http\Requests\WisataRequest;
use Phaza\LaravelPostgis\Geometries\Point;
use Tests\Unit\WisataTest;

class WisatasController extends Controller
{
    public function index(WisataQueryFilter $filters)
    {
        $q = Wisata::query();

        $kuliners = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $kuliners->load('location', 'author');

        $data = WisataResource::collection($kuliners);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('wisata.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Wisata $wisata)
    {
        $wisata->delete();

        if (request()->expectsJson()) {
            return WisataResource::make($wisata);
        }

        return \back();
    }

    public function store(WisataRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $wisata = new Wisata(\array_except($data, ['lat', 'lon', 'published']));
        $wisata = $request->user()->wisatas()->save($wisata);
        $wisata->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return WisataResource::make($wisata)
                ->additional(['redirect' => \route('wisata.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Wisata $wisata, WisataRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $wisata->update(array_except($data, ['lat', 'lon', 'published']));
        if ($wisata->location) {
            $wisata->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $wisata->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return WisataResource::make($wisata)
                ->additional(['redirect' => \route('wisata.index')])
                ->response();
        }

        return \back();
    }

    public function edit(Wisata $wisata)
    {
        return \view('wisata.edit', \compact('wisata'));
    }

    public function create()
    {
        $wisata = new Wisata();

        return view('wisata.create', \compact('wisata'));
    }
}
