<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\OlehOlehQueryFilter;
use App\OlehOleh;
use App\Http\Resources\OlehOlehResource;
use App\Http\Requests\OlehOlehRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class OlehOlehsController extends Controller
{
    public function index(OlehOlehQueryFilter $filters)
    {
        $q = OlehOleh::query();

        $oleholeh = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $oleholeh->load('location', 'author');

        $data = OlehOlehResource::collection($oleholeh);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('oleholeh.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(OlehOleh $oleholeh)
    {
        $oleholeh->delete();

        if (request()->expectsJson()) {
            return OlehOlehResource::make($oleholeh);
        }

        return \back();
    }

    public function store(OlehOlehRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $oleholeh = new OlehOleh(\array_except($data, ['lat', 'lon', 'published']));
        $oleholeh = $request->user()->oleholehs()->save($oleholeh);
        $oleholeh->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return OlehOlehResource::make($oleholeh)
                ->additional(['redirect' => \route('oleholeh.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(OlehOleh $oleholeh, OlehOlehRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $oleholeh->update(array_except($data, ['lat', 'lon', 'published']));
        if ($oleholeh->location) {
            $oleholeh->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $oleholeh->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return OlehOlehResource::make($oleholeh)
                ->additional(['redirect' => \route('oleholeh.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $oleholeh = new OlehOleh();

        return \view('oleholeh.create', \compact('oleholeh'));
    }

    public function edit(OlehOleh $oleholeh)
    {
        return \view('oleholeh.edit', \compact('oleholeh'));
    }
}
