<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wisata;
use Illuminate\Support\Carbon;
use App\Http\Resources\WisataResource;

class PublishingWisataController extends Controller
{
    public function store(Wisata $wisata)
    {
        $wisata->published_at = Carbon::now();
        $wisata->save();

        if (\request()->expectsJson()) {
            return WisataResource::make($wisata)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Wisata $wisata)
    {
        $wisata->published_at = null;
        $wisata->save();

        if (\request()->expectsJson()) {
            return WisataResource::make($wisata);
        }

        return \back();
    }
}
