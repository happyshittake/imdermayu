<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penginapan;
use App\Http\Resources\PenginapanResource;

class PublishingPenginapanController extends Controller
{
    public function store(Penginapan $penginapan)
    {
        $penginapan->published_at = Carbon::now();
        $penginapan->save();

        if (\request()->expectsJson()) {
            return PenginapanResource::make($penginapan)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Penginapan $penginapan)
    {
        $penginapan->published_at = null;
        $penginapan->save();

        if (\request()->expectsJson()) {
            return PenginapanResource::make($penginapan);
        }

        return \back();
    }
}
