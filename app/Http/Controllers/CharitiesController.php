<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\CharityQueryFilter;
use App\Charity;
use App\Http\Resources\CharityResource;
use App\Http\Requests\CharityRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class CharitiesController extends Controller
{
    public function index(CharityQueryFilter $filters)
    {
        $q = Charity::query();

        $charity = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $charity->load('location', 'author');

        $data = CharityResource::collection($charity);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('charity.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Charity $charity)
    {
        $charity->delete();

        if (request()->expectsJson()) {
            return CharityResource::make($charity);
        }

        return \back();
    }

    public function store(CharityRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $charity = new Charity(\array_except($data, ['lat', 'lon', 'published']));
        $charity = $request->user()->charities()->save($charity);
        $charity->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return CharityResource::make($charity)
                ->additional(['redirect' => \route('charity.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Charity $charity, CharityRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $charity->update(array_except($data, ['lat', 'lon', 'published']));
        if ($charity->location) {
            $charity->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $charity->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return CharityResource::make($charity)
                ->additional(['redirect' => \route('charity.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $charity = new Charity();

        return \view('charity.create', \compact('charity'));
    }

    public function edit(Charity $charity)
    {
        return \view('charity.edit', \compact('charity'));
    }
}
