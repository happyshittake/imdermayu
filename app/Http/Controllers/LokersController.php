<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\LokerQueryFilter;
use App\Loker;
use App\Http\Resources\LokerResource;
use App\Http\Requests\LokerRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class LokersController extends Controller
{
    public function index(LokerQueryFilter $filters)
    {
        $q = Loker::query();

        $kuliners = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $kuliners->load('location', 'author');

        $data = LokerResource::collection($kuliners);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('loker.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Loker $loker)
    {
        $loker->delete();

        if (request()->expectsJson()) {
            return LokerResource::make($loker);
        }

        return \back();
    }

    public function store(LokerRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $loker = new Loker(\array_except($data, ['lat', 'lon', 'published']));
        $loker = $request->user()->lokers()->save($loker);
        $loker->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return LokerResource::make($loker)
                ->additional(['redirect' => \route('loker.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Loker $loker, LokerRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $loker->update(array_except($data, ['lat', 'lon', 'published']));
        if ($loker->location) {
            $loker->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $loker->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return LokerResource::make($loker)
                ->additional(['redirect' => \route('loker.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $loker = new Loker();

        return view('loker.create', \compact('loker'));
    }

    public function edit(Loker $loker) {
        return view('loker.edit', \compact('loker'));
    }
}
