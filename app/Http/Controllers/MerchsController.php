<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\MerchQueryFilter;
use App\Merch;
use App\Http\Resources\MerchResource;
use App\Http\Requests\MerchRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class MerchsController extends Controller
{
    public function index(MerchQueryFilter $filters)
    {
        $q = Merch::query();

        $merch = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $merch->load('location', 'author');

        $data = MerchResource::collection($merch);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('merch.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Merch $merch)
    {
        $merch->delete();

        if (request()->expectsJson()) {
            return MerchResource::make($merch);
        }

        return \back();
    }

    public function store(MerchRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $merch = new Merch(\array_except($data, ['lat', 'lon', 'published']));
        $merch = $request->user()->merchs()->save($merch);
        $merch->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return MerchResource::make($merch)
                ->additional(['redirect' => \route('merch.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Merch $merch, MerchRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $merch->update(array_except($data, ['lat', 'lon', 'published']));
        if ($merch->location) {
            $merch->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $merch->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return MerchResource::make($merch)
                ->additional(['redirect' => \route('merch.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $merch = new Merch();

        return \view('merch.create', \compact('merch'));
    }

    public function edit(Merch $merch)
    {
        return \view('merch.edit', \compact('merch'));
    }
}
