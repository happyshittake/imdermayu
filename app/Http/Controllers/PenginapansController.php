<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\QueryFilters\PenginapanQueryFilter;
use App\Penginapan;
use App\Http\Resources\PenginapanResource;
use App\Http\Requests\PenginapanRequest;
use Phaza\LaravelPostgis\Geometries\Point;

class PenginapansController extends Controller
{
    public function index(PenginapanQueryFilter $filters)
    {
        $q = Penginapan::query();

        $penginapan = $q->filterBy($filters)
            ->orderBy('published_at', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate(12);

        $penginapan->load('location', 'author');

        $data = PenginapanResource::collection($penginapan);

        if (request()->expectsJson()) {
            return $data;
        }

        return view('penginapan.index', ['data' => $data->resource->toArray()]);
    }

    public function destroy(Penginapan $penginapan)
    {
        $penginapan->delete();

        if (request()->expectsJson()) {
            return PenginapanResource::make($penginapan);
        }

        return \back();
    }

    public function store(PenginapanRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $penginapan = new Penginapan(\array_except($data, ['lat', 'lon', 'published']));
        $penginapan = $request->user()->penginapans()->save($penginapan);
        $penginapan->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        \DB::commit();

        if ($request->expectsJson()) {
            return PenginapanResource::make($penginapan)
                ->additional(['redirect' => \route('penginapan.index')])
                ->response()
                ->setStatusCode(201);
        }

        return \back();
    }

    public function update(Penginapan $penginapan, PenginapanRequest $request)
    {
        $data = $request->validated();

        if (!$data['published']) {
            $data['published_at'] = null;
        }

        \DB::beginTransaction();
        $penginapan->update(array_except($data, ['lat', 'lon', 'published']));
        if ($penginapan->location) {
            $penginapan->location->update(['point' => new Point($data['lat'], $data['lon'])]);
        } else {
            $penginapan->location()->create(['point' => new Point($data['lat'], $data['lon'])]);
        }
        \DB::commit();

        if ($request->expectsJson()) {
            return PenginapanResource::make($penginapan)
                ->additional(['redirect' => \route('penginapan.index')])
                ->response();
        }

        return \back();
    }

    public function create()
    {
        $penginapan = new Penginapan();

        return \view('penginapan.create', \compact('penginapan'));
    }

    public function edit(Penginapan $penginapan)
    {
        return \view('penginapan.edit', \compact('penginapan'));
    }
}
