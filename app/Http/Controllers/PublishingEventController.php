<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Http\Resources\EventResource;

class PublishingEventController extends Controller
{
    public function store(Event $event)
    {
        $event->published_at = Carbon::now();
        $event->save();

        if (\request()->expectsJson()) {
            return EventResource::make($event)->response()->setStatusCode(201);
        }

        return \back();
    }

    public function destroy(Event $event)
    {
        $event->published_at = null;
        $event->save();

        if (\request()->expectsJson()) {
            return EventResource::make($event);
        }

        return \back();
    }
}
