<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use App\Http\Resources\UserResource;

class UsersStatusController extends Controller
{
    public function store(User $user)
    {
        $user->is_active = true;
        $user->save();

        if (\request()->expectsJson()) {
            return UserResource::make($user);
        }

        return \back();
    }

    public function destroy(User $user)
    {
        $user->is_active = false;
        $user->save();

        if (\request()->expectsJson()) {
            return UserResource::make($user);
        }

        return \back();
    }
}
