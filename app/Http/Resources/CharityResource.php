<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CharityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'image' => $this->image,
            'published' => $this->published,
            'address' => $this->address,
            'bank_name' => $this->bank_name,
            'bank_account_no' => $this->bank_account_no,
            'bank_account_owner' => $this->bank_account_owner,
            'contact' => $this->contact,
            'published_at' => optional($this->published_at)->format("Y-m-d H:i:s"),
            'path' => config('app.url') . "/charities",
            'author' => UserResource::make($this->whenLoaded('author')),
            'location' => LocationResource::make($this->whenLoaded('location'))
        ];
    }
}
