<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class KulinerResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'image' => $this->image,
            'price' => $this->price,
            'open_time' => $this->open_time,
            'address' => $this->address,
            'close_time' => $this->close_time,
            'published' => $this->published,
            'published_at' => optional($this->published_at)->format("Y-m-d H:i:s"),
            'path' => config('app.url') . "/kuliners",
            'author' => UserResource::make($this->whenLoaded('author')),
            'location' => LocationResource::make($this->whenLoaded('location'))
        ];
    }
}
