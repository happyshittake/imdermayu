<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\UserResource;
use App\Http\Resources\LocationResource;

class WisataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body' => $this->body,
            'image' => $this->image,
            'address' => $this->address,
            'published' => $this->published,
            'published_at' => optional($this->published_at)->format("Y-m-d H:i:s"),
            'path' => config('app.url') . "/wisatas",
            'author' => UserResource::make($this->whenLoaded('author')),
            'location' => LocationResource::make($this->whenLoaded('location'))
        ];
    }
}
