<?php 
namespace App\Http\QueryFilters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class QueryFilter
{
    protected $request;
    protected $query;
    protected $implicitFilters = [];


    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public static function hydrate(array $queries) : QueryFilters
    {
        $request = new Request($queries);
        return new static($request);
    }

    public function applyToQuery(Builder $query) : Builder
    {
        $this->query = $query;
        foreach ($this->request->all() as $filter => $value) {
            $method = Str::camel($filter);
            if ($this->filterCanBeApplied($method, $value)) {
                call_user_func([$this, $method], $value);
            }
        }
        return $query;
    }

    protected function filterCanBeApplied($filter, $value)
    {
        $filterExists = method_exists($this, $filter);
        $hasValue = $value !== '' && $value !== null;
        $valueIsLegit = $hasValue || in_array($filter, $this->implicitFilters);
        return $filterExists && $valueIsLegit;
    }
}

