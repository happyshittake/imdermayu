<?php 
namespace App\Http\QueryFilters;

use Carbon\Carbon;
use App\Wisata;


class WisataQueryFilter extends QueryFilter
{
    public function q($v)
    {
        $keys = Wisata::search($v)->keys();

        $this->query->whereIn('id', $keys->all());
    }

    public function f($v = 'all')
    {
        switch ($v) {
            case 'unpublished':
                $this->query->where(function ($query) {
                    $query->whereNull('published_at')->orWhere('published_at', '>', Carbon::now());
                });
                break;
            case 'published':
                $this->query->where(function ($query) {
                    $query->whereNotNull('published_at')->where('published_at', '<=', Carbon::now());
                });
                break;
        }
    }
}