<?php
namespace App\Http\QueryFilters;

use Illuminate\Support\Carbon;
use App\OlehOleh;
use App\Merch;

class MerchQueryFilter extends QueryFilter
{
    public function q($v)
    {
        $keys = Merch::search($v)->keys();

        $this->query->whereIn('id', $keys->all());
    }

    public function f($v = 'all')
    {
        switch ($v) {
            case 'unpublished':
                $this->query->where(function ($query) {
                    $query->whereNull('published_at')->orWhere('published_at', '>', Carbon::now());
                });
                break;
            case 'published':
                $this->query->where(function ($query) {
                    $query->whereNotNull('published_at')->where('published_at', '<=', Carbon::now());
                });
                break;
        }
    }

    public function type($t)
    {
        switch ($t) {
            case 'po':
                $this->query->where('ready_stock', false);
                break;
            case 'ready':
                $this->query->where('ready_stock', true);
                break;
        }
    }
}