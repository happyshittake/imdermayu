<?php
namespace App\Http\QueryFilters;

use Illuminate\Support\Carbon;
use App\OlehOleh;
use App\Penginapan;

class PenginapanQueryFilter extends QueryFilter
{
    public function q($v)
    {
        $keys = Penginapan::search($v)->keys();

        $this->query->whereIn('id', $keys->all());
    }

    public function f($v = 'all')
    {
        switch ($v) {
            case 'unpublished':
                $this->query->where(function ($query) {
                    $query->whereNull('published_at')->orWhere('published_at', '>', Carbon::now());
                });
                break;
            case 'published':
                $this->query->where(function ($query) {
                    $query->whereNotNull('published_at')->where('published_at', '<=', Carbon::now());
                });
                break;
        }
    }
}