<?php

namespace App\Http\QueryFilters\Traits;

use App\Http\QueryFilters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

trait FiltersRecords
{
    /**
     * @param $query
     * @param \App\QueryFilters\QueryFilters $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilterBy($query, QueryFilter $filters): Builder
    {
        return $filters->applyToQuery($query);
    }
}