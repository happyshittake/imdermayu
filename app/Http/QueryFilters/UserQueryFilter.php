<?php 
namespace App\Http\QueryFilters;

use Carbon\Carbon;
use App\User;


class UserQueryFilter extends QueryFilter
{
    public function q($v)
    {
        $keys = User::search($v)->keys();

        $this->query->whereIn('id', $keys->all());
    }

    public function f($v = 'all')
    {
        switch ($v) {
            case 'active':
                $this->query->where('is_active', true);
                break;
            case 'inactive':
                $this->query->where('is_active', false);
                break;
        }
    }
}