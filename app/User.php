<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Kuliner;
use App\Wisata;
use App\Loker;
use Laravel\Scout\Searchable;
use App\Http\QueryFilters\Traits\FiltersRecords;

class User extends Authenticatable
{
    use Notifiable, Searchable, FiltersRecords;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_active'
    ];

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'is_active' => $this->is_active
        ];
    }

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'email' => $this->email
        ];
    }

    public function searchableOptions()
    {
        return [
            'rank' => [
                'fields' => [
                    'name' => 'A',
                    'email' => 'B'
                ]
            ]
        ];
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function kuliners()
    {
        return $this->hasMany(Kuliner::class, 'author_id');
    }

    public function wisatas()
    {
        return $this->hasMany(Wisata::class, 'author_id');
    }

    public function lokers()
    {
        return $this->hasMany(Loker::class, 'author_id');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'author_id');
    }

    public function oleholehs()
    {
        return $this->hasMany(OlehOleh::class, 'author_id');
    }

    public function merchs()
    {
        return $this->hasMany(Merch::class, 'author_id');
    }

    public function penginapans()
    {
        return $this->hasMany(Penginapan::class, 'author_id');
    }

    public function charities()
    {
        return $this->hasMany(Charity::class, 'author_id');
    }
}
