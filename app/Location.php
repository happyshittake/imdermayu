<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Phaza\LaravelPostgis\Eloquent\PostgisTrait;

class Location extends Model
{
    use PostgisTrait;

    protected $guarded = [];

    protected $postgisFields = [
        'point'
    ];

    protected $postgisTypes = [
        'point' => [
            'geomtype' => 'geography',
            'srid' => 4326
        ]
    ];

    public function content()
    {
        return $this->morphTo('location');
    }
}
