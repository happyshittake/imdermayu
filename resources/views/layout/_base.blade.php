<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMDermayu Admin - @yield('title')</title>
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
	<div id="app">
		<nav class="navbar is-dark" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="https://bulma.io">
					<img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
				</a>
			</div>
		</nav>
		<section class="section">
			<div class="columns">
				<div class="column is-2">
					@include('partials._nav')
				</div>
				<div class="column">
					@yield('body')
				</div>
			</div>
		</section>
	</div>
	<script src="{{ mix('js/app.js') }}"></script>
</body>

</html>