@extends('layout._base') 
@section('title', 'Buat oleholeh Baru')
@section('body')
<oleholeh-create inline-template :data='@json($oleholeh->toArray())' v-cloak>
    <form-oleholeh :data="data" :status="formStatus" @submit-form="handleSubmit"></form-oleholeh>
</oleholeh-create>
@endsection 