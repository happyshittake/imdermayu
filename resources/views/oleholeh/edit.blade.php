@extends('layout._base') 
@section('title', 'Edit oleholeh')
@section('body')
<oleholeh-edit inline-template :data='@json($oleholeh->toArray())' v-cloak>
    <form-oleholeh :data="data" :status="formStatus" @submit-form="handleSubmit"></form-oleholeh>
</oleholeh-edit>
@endsection