@extends('layout._base') 
@section('title', 'oleholeh') 
@section('body')
<oleholeh-index inline-template :data='@json($data['data'])' :meta='@json([
                                    'path' => $data['path'],
                                    'currentPage' => $data['current_page'],
                                    'lastPage' => $data['last_page']
                                ])' q="{{request('q')}}" v-cloak>
	<div>

		<modal v-if="deleteTarget" :is-active="deleteTarget != null" @modal-close="deleteTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan menghapus oleholeh ${deleteTarget.title}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="deleteTarget = null">Cancel</a>
					<a class="card-footer-item" @click="destroyTarget">Delete</a>
				</footer>
			</div>
		</modal>

		<modal v-if="publishingTarget" :is-active="publishingTarget != null" @modal-close="publishingTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan merubah publikasi oleholeh ${publishingTarget.title}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="publishingTarget = null">Cancel</a>
					<a class="card-footer-item" @click="publishTarget">
						@{{ publishingTarget.published ? 'Unpublish' : 'Publish' }}
					</a>
				</footer>
			</div>
		</modal>

		<div class="level">
			<div class="level-left">
				<div class="level-item">
					<a href="/oleholehs/create" class="button is-primary">
						New oleholeh
					</a>
				</div>
				<div class="level-item">
					<search-field @search-query="handleSearch" :q="query"></search-field>
				</div>
			</div>

			<div class="level-right">
				<div class="level-item">
					<filter-field :status="['all', 'published', 'unpublished']" @filter-selected="handleFilter" :value="filter"></filter-field>
				</div>
			</div>
		</div>

		<div class="columns is-multiline">
			<div class="column is-one-quarter-desktop is-full-mobile" v-for="card in oleholehs" :key="card.slug">
				<oleholeh-card :data="card" @delete-oleholeh="handleDelete" @toggle-publish="v => publishingTarget = v"></oleholeh-card>
			</div>
		</div>


		<pagination :path="metaData.path" :current-page="metaData.currentPage" :last-page="metaData.lastPage" :query-param="queryObj">
		</pagination>
	</div>

</oleholeh-index>
@endsection