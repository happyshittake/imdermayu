<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMDermayu Admin</title>
    <link rel="stylesheet" href="{{ mix('app.css') }}">
</head>
<body>
<section class="section">
    <div class="columns">
        <div class="column">
            <nav class="panel">
                <a href="/" class="panel-block">
                    Home
                </a>
                <a href="/kuliner" class="panel-block">
                    Kuliner
                </a>
            </nav>
        </div>
    </div>
</section>
<script src="{{ mix('app.js') }}"></script>
</body>
</html>
