@extends('layout._base') 
@section('title', 'Buat penginapan Baru')
@section('body')
<penginapan-create inline-template :data='@json($penginapan->toArray())' v-cloak>
    <form-penginapan :data="data" :status="formStatus" @submit-form="handleSubmit"></form-penginapan>
</penginapan-create>
@endsection 