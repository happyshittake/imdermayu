@extends('layout._base') 
@section('title', 'Edit penginapan')
@section('body')
<penginapan-edit inline-template :data='@json($penginapan->toArray())' v-cloak>
    <form-penginapan :data="data" :status="formStatus" @submit-form="handleSubmit"></form-penginapan>
</penginapan-edit>
@endsection