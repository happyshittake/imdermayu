@extends('layout._base') 
@section('title', 'Buat Kuliner Baru')
@section('body')
<kuliner-create inline-template :data='@json($kuliner->toArray())' v-cloak>
    <form-kuliner :data="data" :status="formStatus" @submit-form="handleSubmit"></form-kuliner>
</kuliner-create>
@endsection 