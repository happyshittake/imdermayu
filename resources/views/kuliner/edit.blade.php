@extends('layout._base') 
@section('title', 'edit kuliner')
@section('body')
<kuliner-edit inline-template :data='@json($kuliner->toArray())' v-cloak>
    <form-kuliner :data="data" :status="formStatus" @submit-form="handleSubmit"></form-kuliner>
</kuliner-edit>
@endsection 