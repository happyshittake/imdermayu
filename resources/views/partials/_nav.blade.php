<nav class="panel">
	<a href="/" class="panel-block">
		Home
	</a>
	<a href="/kuliners" class="panel-block">
		Kuliner
	</a>
	<a href="/wisatas" class="panel-block">
		Wisata & Hiburan
	</a>
	<a href="/lokers" class="panel-block">
		Loker
	</a>
	<a href="/events" class="panel-block">
		Event
	</a>
	<a href="/oleholehs" class="panel-block">
		Oleh - oleh
	</a>
	<a href="/merchs" class="panel-block">
		Merchandise
	</a>
	<a href="/penginapans" class="panel-block">
		Penginapan
	</a>
	<a href="/charities" class="panel-block">
		IM Peduli
	</a>
	<a href="/users" class="panel-block">
		User
	</a>
	<a href="/medias" class="panel-block">
		Media
	</a>
</nav>