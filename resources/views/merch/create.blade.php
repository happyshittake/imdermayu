@extends('layout._base') 
@section('title', 'Buat merch Baru')
@section('body')
<merch-create inline-template :data='@json($merch->toArray())' v-cloak>
    <form-merch :data="data" :status="formStatus" @submit-form="handleSubmit"></form-merch>
</merch-create>
@endsection 