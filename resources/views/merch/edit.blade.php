@extends('layout._base') 
@section('title', 'Edit merch')
@section('body')
<merch-edit inline-template :data='@json($merch->toArray())' v-cloak>
    <form-merch :data="data" :status="formStatus" @submit-form="handleSubmit"></form-merch>
</merch-edit>
@endsection