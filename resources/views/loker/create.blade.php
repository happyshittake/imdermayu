@extends('layout._base') 
@section('title', 'Buat Loker Baru')
@section('body')
<loker-create inline-template :data='@json($loker->toArray())' v-cloak>
    <form-loker :data="data" :status="formStatus" @submit-form="handleSubmit"></form-loker>
</loker-create>
@endsection 