@extends('layout._base') 
@section('title', 'Loker') 
@section('body')
<loker-index inline-template :data='@json($data['data'])' :meta='@json([
                                    'path' => $data['path'],
                                    'currentPage' => $data['current_page'],
                                    'lastPage' => $data['last_page']
                                ])' q="{{request('q')}}" v-cloak>
	<div>

		<modal v-if="deleteTarget" :is-active="deleteTarget != null" @modal-close="deleteTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan menghapus loker ${deleteTarget.title}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="deleteTarget = null">Cancel</a>
					<a class="card-footer-item" @click="destroyTarget">Delete</a>
				</footer>
			</div>
		</modal>

		<modal v-if="publishingTarget" :is-active="publishingTarget != null" @modal-close="publishingTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan merubah publikasi loker ${publishingTarget.title}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="publishingTarget = null">Cancel</a>
					<a class="card-footer-item" @click="publishTarget">
						@{{ publishingTarget.published ? 'Unpublish' : 'Publish' }}
					</a>
				</footer>
			</div>
		</modal>

		<div class="level">
			<div class="level-left">
				<div class="level-item">
					<a href="/lokers/create" class="button is-primary">
						New Loker
					</a>
				</div>
				<div class="level-item">
					<search-field @search-query="handleSearch" :q="query"></search-field>
				</div>
			</div>

			<div class="level-right">
				<div class="level-item">
					<filter-field :status="['all', 'published', 'unpublished']" @filter-selected="handleFilter" :value="filter"></filter-field>
				</div>
			</div>
		</div>

		<div class="columns is-multiline">
			<div class="column is-one-quarter-desktop is-full-mobile" v-for="card in lokers" :key="card.slug">
				<loker-card :data="card" @delete-loker="handleDelete" @toggle-publish="v => publishingTarget = v"></loker-card>
			</div>
		</div>


		<pagination :path="metaData.path" :current-page="metaData.currentPage" :last-page="metaData.lastPage" :query-param="queryObj">
		</pagination>
	</div>

</loker-index>
@endsection