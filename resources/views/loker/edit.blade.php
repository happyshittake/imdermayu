@extends('layout._base') 
@section('title', 'Edit Wisata')
@section('body')
<loker-edit inline-template :data='@json($loker->toArray())' v-cloak>
    <form-loker :data="data" :status="formStatus" @submit-form="handleSubmit"></form-loker>
</loker-edit>
@endsection