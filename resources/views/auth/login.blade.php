<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IMDermayu Admin - Login</title>
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
	<section class="section">
		<div class="columns is-centered">
			<div class="column is-half">
				@if($errors->any())
				<div class="notification is-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<form method="post" action="{{ action('Auth\LoginController@login') }}">
					{{ csrf_field() }}
					<div class="field">
						<label for="email" class="label">Email</label>
						<div class="control">
							<input type="email" id="email" name="email" class="input" placeholder="Email..." required>
						</div>
					</div>
					<div class="field">
						<label for="password" class="label">Password</label>
						<div class="control">
							<input type="password" id="password" name="password" class="input" required>
						</div>
					</div>
					<div class="field">
						<label class="checkbox">
							<input type="checkbox" name="remember"> Remember
						</label>
					</div>
					<div class="field">
						<button type="submit" class="button is-primary is-fullwidth">Login</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</body>

</html>