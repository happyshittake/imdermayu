@extends('layout._base') 
@section('title', 'Edit Wisata')
@section('body')
<wisata-edit inline-template :data='@json($wisata->toArray())' v-cloak>
    <form-wisata :data="data" :status="formStatus" @submit-form="handleSubmit"></form-wisata>
</wisata-edit>
@endsection