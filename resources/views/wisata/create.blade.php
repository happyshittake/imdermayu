@extends('layout._base') 
@section('title', 'Buat Wisata Baru')
@section('body')
<wisata-create inline-template :data='@json($wisata->toArray())' v-cloak>
    <form-wisata :data="data" :status="formStatus" @submit-form="handleSubmit"></form-wisata>
</wisata-create>
@endsection 