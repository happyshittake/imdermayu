@extends('layout._base') 
@section('title', 'Edit charity')
@section('body')
<charity-edit inline-template :data='@json($charity->toArray())' v-cloak>
    <form-charity :data="data" :status="formStatus" @submit-form="handleSubmit"></form-charity>
</charity-edit>
@endsection