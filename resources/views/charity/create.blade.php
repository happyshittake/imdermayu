@extends('layout._base') 
@section('title', 'Buat charity Baru')
@section('body')
<charity-create inline-template :data='@json($charity->toArray())' v-cloak>
    <form-charity :data="data" :status="formStatus" @submit-form="handleSubmit"></form-charity>
</charity-create>
@endsection 