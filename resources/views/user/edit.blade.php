@extends('layout._base') 
@section('title', 'Edit User')
@section('body')
<user-edit inline-template :data='@json($user->toArray())' v-cloak>
    <form-user :data="data" :status="formStatus" @submit-form="handleSubmit"></form-user>
</user-edit>
@endsection