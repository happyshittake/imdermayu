@extends('layout._base') 
@section('title', 'Buat User Baru')
@section('body')
<user-create inline-template :data='@json($user->toArray())' v-cloak>
    <form-user :data="data" :status="formStatus" @submit-form="handleSubmit"></form-user>
</loker-create>
@endsection 