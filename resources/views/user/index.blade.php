@extends('layout._base') 
@section('title', 'Loker') 
@section('body')
<user-index inline-template :data='@json($data['data'])' :meta='@json([
                                    'path' => $data['path'],
                                    'currentPage' => $data['current_page'],
                                    'lastPage' => $data['last_page']
                                ])' q="{{request('q')}}" v-cloak>
	<div>

		<modal v-if="activationTarget" :is-active="activationTarget != null" @modal-close="activationTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan merubah activasi user ${activationTarget.name}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="activationTarget = null">Cancel</a>
					<a class="card-footer-item" @click="activateTarget">
						@{{ activationTarget.is_active ? 'Deactivate' : 'Activate' }}
					</a>
				</footer>
			</div>
		</modal>

		<div class="level">
			<div class="level-left">
				<div class="level-item">
					<a href="/users/create" class="button is-primary">
						New user
					</a>
				</div>
				<div class="level-item">
					<search-field @search-query="handleSearch" :q="query"></search-field>
				</div>
			</div>

			<div class="level-right">
				<div class="level-item">
					<filter-field :status="['all', 'active', 'inactive']" @filter-selected="handleFilter" :value="filter"></filter-field>
				</div>
			</div>
		</div>

		<div class="columns is-multiline">
			<div class="column is-one-quarter-desktop is-full-mobile" v-for="card in users" :key="card.slug">
				<user-card :data="card" @toggle-activate="v => activationTarget = v"></user-card>
			</div>
		</div>


		<pagination :path="metaData.path" :current-page="metaData.currentPage" :last-page="metaData.lastPage" :query-param="queryObj">
		</pagination>
	</div>

</user-index>
@endsection