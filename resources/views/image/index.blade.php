@extends('layout._base') 
@section('title', 'Image') 
@section('body')
<image-index inline-template :data='@json($data['data'])' 
                             :meta='@json([
                                'path' => $data['path'],
                                'currentPage' => $data['current_page'],
                                'lastPage' => $data['last_page']
                             ])' v-cloak>
	<div>
		<modal :is-active="activeModal" @modal-close="activeModal = false">
			<file-upload @file-uploaded="handleFileUploaded"></file-upload>
		</modal>

		<modal v-if="deleteTarget" :is-active="deleteTarget != null" @modal-close="deleteTarget = null">
			<div class="card">
				<div class="card-content">
					@{{ `Anda akan menghapus image ${deleteTarget.name}, anda yakin?` }}
				</div>

				<footer class="card-footer">
					<a class="card-footer-item" @click="deleteTarget = null">Cancel</a>
					<a class="card-footer-item" @click="destroyTarget">Delete</a>
				</footer>
			</div>
		</modal>


		<div class="level">
			<div class="level-left">
				<div class="level-item">
					<button class="button is-primary" @click="activeModal = true">
						Upload Image
					</button>
				</div>
			</div>
		</div>

		<div class="columns is-multiline">
			<div class="column is-one-quarter-desktop is-full-mobile" v-for="card in images" :key="card.name">
				<image-card :img-src="card.url" 
							:img-caption="card.caption" 
							:img-name="card.name" 
							@delete-image="deleteTarget = card" 
							@submit-caption="saveCaption">
				</image-card>
			</div>
		</div>

		<pagination :path="metaData.path" :current-page="metaData.currentPage" :last-page="metaData.lastPage">
		</pagination>
	</div>
</image-index>
@endsection