@extends('layout._base') 
@section('title', 'Edit event')
@section('body')
<event-edit inline-template :data='@json($event->toArray())' v-cloak>
    <form-event :data="data" :status="formStatus" @submit-form="handleSubmit"></form-event>
</event-edit>
@endsection