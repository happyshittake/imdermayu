@extends('layout._base') 
@section('title', 'Buat Event Baru')
@section('body')
<event-create inline-template :data='@json($event->toArray())' v-cloak>
    <form-event :data="data" :status="formStatus" @submit-form="handleSubmit"></form-event>
</event-create>
@endsection 