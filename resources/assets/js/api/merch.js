import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/merchs";

export function fetchMerchsPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteMerch(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitMerch(data) {
  return axios.post(BASEPATH, data);
}

export function updateMerch(merchID, data) {
  return axios.patch(`${BASEPATH}/${merchID}`, data);
}

export function publishMerch(id) {
  return axios.post(`/publishing/merchs/${id}`);
}

export function unpublishMerch(id) {
  return axios.delete(`/publishing/merchs/${id}`);
}
