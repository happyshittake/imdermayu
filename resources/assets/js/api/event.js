import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/events";

export function fetchEventsPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteEvent(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitEvent(data) {
  return axios.post(BASEPATH, data);
}

export function updateEvent(eventId, data) {
  return axios.patch(`${BASEPATH}/${eventId}`, data);
}

export function publishEvent(id) {
  return axios.post(`/publishing/events/${id}`);
}

export function unpublishEvent(id) {
  return axios.delete(`/publishing/events/${id}`);
}
