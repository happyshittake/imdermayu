const axios = window.axios;

export function updateCaption(name, caption = "") {
  return axios.patch(`/medias/${name}`, { caption });
}

export function uploadImage(file, config = null) {
  const formData = new FormData();
  formData.append("file", file, file.name);

  return axios.post("/medias", formData, config);
}

export function fetchImagePerPage(page = 1) {
  return axios.get(`/medias?page=${page}`);
}

export function deleteImage(name) {
  return axios.delete(`/medias/${name}`);
}
