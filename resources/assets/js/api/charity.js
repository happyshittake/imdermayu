import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/charities";

export function fetchCharitiesPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteCharity(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitCharity(data) {
  return axios.post(BASEPATH, data);
}

export function updateCharity(charityID, data) {
  return axios.patch(`${BASEPATH}/${charityID}`, data);
}

export function publishCharity(id) {
  return axios.post(`/publishing/charities/${id}`);
}

export function unpublishCharity(id) {
  return axios.delete(`/publishing/charities/${id}`);
}
