import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/lokers";

export function fetchLokersPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteLoker(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitLoker(data) {
  return axios.post(BASEPATH, data);
}

export function updateLoker(lokerId, data) {
  return axios.patch(`${BASEPATH}/${lokerId}`, data);
}

export function publishLoker(id) {
  return axios.post(`/publishing/lokers/${id}`);
}

export function unpublishLoker(id) {
  return axios.delete(`/publishing/lokers/${id}`);
}
