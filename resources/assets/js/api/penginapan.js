import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/penginapans";

export function fetchPenginapansPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deletePenginapan(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitPenginapan(data) {
  return axios.post(BASEPATH, data);
}

export function updatePenginapan(penginapanID, data) {
  return axios.patch(`${BASEPATH}/${penginapanID}`, data);
}

export function publishPenginapan(id) {
  return axios.post(`/publishing/penginapans/${id}`);
}

export function unpublishPenginapan(id) {
  return axios.delete(`/publishing/penginapans/${id}`);
}
