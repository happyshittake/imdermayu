import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/kuliners";

export function fetchKulinersPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteKuliner(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitKuliner(data) {
  return axios.post(BASEPATH, data);
}

export function updateKuliner(kulinerId, data) {
  return axios.patch(`${BASEPATH}/${kulinerId}`, data);
}

export function publishKuliner(id) {
  return axios.post(`/publishing/kuliners/${id}`);
}

export function unpublishKuliner(id) {
  return axios.delete(`/publishing/kuliners/${id}`);
}
