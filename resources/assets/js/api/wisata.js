import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/wisatas";

export function fetchWisatasPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteWisata(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitWisata(data) {
  return axios.post(BASEPATH, data);
}

export function updateWisata(wisataId, data) {
  return axios.patch(`${BASEPATH}/${wisataId}`, data);
}

export function publishWisata(id) {
  return axios.post(`/publishing/wisatas/${id}`);
}

export function unpublishWisata(id) {
  return axios.delete(`/publishing/wisatas/${id}`);
}
