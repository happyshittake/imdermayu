import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/users";

export function fetchUsersPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function submitUser(data) {
  return axios.post(BASEPATH, data);
}

export function updateUser(userId, data) {
  return axios.patch(`${BASEPATH}/${userId}`, data);
}

export function activateUser(id) {
  return axios.post(`/activation/users/${id}`);
}

export function deactivateUser(id) {
  return axios.delete(`/activation/users/${id}`);
}
