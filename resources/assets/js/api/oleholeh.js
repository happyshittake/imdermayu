import URI from "urijs";

const axios = window.axios;

export const BASEPATH = "/oleholehs";

export function fetchOlehOlehsPerPage(page = 1, q = "", f = "all") {
  return axios.get(URI(BASEPATH).query({ page, q, f }));
}

export function deleteOlehOleh(id) {
  return axios.delete(`${BASEPATH}/${id}`);
}

export function submitOlehOleh(data) {
  return axios.post(BASEPATH, data);
}

export function updateOlehOleh(olehOlehId, data) {
  return axios.patch(`${BASEPATH}/${olehOlehId}`, data);
}

export function publishOlehOleh(id) {
  return axios.post(`/publishing/oleholehs/${id}`);
}

export function unpublishOlehOleh(id) {
  return axios.delete(`/publishing/oleholehs/${id}`);
}
