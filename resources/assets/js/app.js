import { install as VueGoogleMaps } from "vue2-google-maps";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("image-index", require("./views/ImageIndexPage.vue"));

Vue.component("kuliner-index", require("./views/KulinerIndexPage.vue"));
Vue.component("kuliner-edit", require("./views/KulinerEditPage.vue"));
Vue.component("kuliner-create", require("./views/KulinerCreatePage.vue"));

Vue.component("wisata-index", require("./views/WisataIndexPage.vue"));
Vue.component("wisata-create", require("./views/WisataCreatePage.vue"));
Vue.component("wisata-edit", require("./views/WisataEditPage.vue"));

Vue.component("loker-index", require("./views/LokerIndexPage.vue"));
Vue.component("loker-create", require("./views/LokerCreatePage.vue"));
Vue.component("loker-edit", require("./views/LokerEditPage.vue"));

Vue.component("user-index", require("./views/UserIndexPage.vue"));
Vue.component("user-create", require("./views/UserCreatePage.vue"));
Vue.component("user-edit", require("./views/UserEditPage.vue"));

Vue.component("event-index", require("./views/EventIndexPage.vue"));
Vue.component("event-create", require("./views/EventCreatePage.vue"));
Vue.component("event-edit", require("./views/EventEditPage.vue"));

Vue.component("oleholeh-index", require("./views/OlehOlehIndexPage.vue"));
Vue.component("oleholeh-create", require("./views/OlehOlehCreatePage.vue"));
Vue.component("oleholeh-edit", require("./views/OlehOlehEditPage.vue"));

Vue.component("merch-index", require("./views/MerchIndexPage.vue"));
Vue.component("merch-create", require("./views/MerchCreatePage.vue"));
Vue.component("merch-edit", require("./views/MerchEditPage.vue"));

Vue.component("penginapan-index", require("./views/PenginapanIndexPage.vue"));
Vue.component("penginapan-create", require("./views/PenginapanCreatePage.vue"));
Vue.component("penginapan-edit", require("./views/PenginapanEditPage.vue"));

Vue.component("charity-index", require("./views/CharityIndexPage.vue"));
Vue.component("charity-create", require("./views/CharityCreatePage.vue"));
Vue.component("charity-edit", require("./views/CharityEditPage.vue"));

Vue.filter("capitalize", v => {
  return v.charAt(0).toUpperCase() + v.slice(1).toLowerCase();
});

Vue.use(VueGoogleMaps, {
  load: {
    key: process.env.MIX_GOOGLE_MAPS_KEY,
    libraries: "places"
  }
});

const app = new Vue({
  el: "#app"
});

window.Bus = new Vue();
