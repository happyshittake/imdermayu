<?php
namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Loker;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Location;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class LokerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $loker = factory(Loker::class)->create([
            'title' => $name
        ]);

        $loker2 = factory(Loker::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($loker->slug, $loker2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $loker = factory(Loker::class)->create();

        $loker->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $loker->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $loker = factory(Loker::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $loker->author);

    }
}
