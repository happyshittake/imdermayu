<?php
namespace Tests\Unit;

use App\Kuliner;
use App\Location;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Phaza\LaravelPostgis\Geometries\Point;
use Tests\TestCase;

class KulinerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_can_accept_minute_and_second_as_open_and_close_time()
    {
        $openTime = '01:00';
        $closeTime = '23:00';

        $kuliner = factory(Kuliner::class)->create([
            'open_time' => $openTime,
            'close_time' => $closeTime
        ]);

        $this->assertEquals($openTime, $kuliner->open_time);
        $this->assertEquals($closeTime, $kuliner->close_time);
    }

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $kuliner = factory(Kuliner::class)->create([
            'title' => $name
        ]);

        $kuliner2 = factory(Kuliner::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($kuliner->slug, $kuliner2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $kuliner = factory(Kuliner::class)->create();

        $kuliner->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $kuliner->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $kuliner = factory(Kuliner::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $kuliner->author);

    }
}
