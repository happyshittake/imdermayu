<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Wisata;
use Phaza\LaravelPostgis\Geometries\Point;
use App\User;
use App\Location;

class WisataTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $wisata = factory(Wisata::class)->create([
            'title' => $name
        ]);

        $wisata2 = factory(Wisata::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($wisata->slug, $wisata2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $wisata = factory(Wisata::class)->create();

        $wisata->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $wisata->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $wisata = factory(Wisata::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $wisata->author);

    }
}
