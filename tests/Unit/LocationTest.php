<?php

namespace Tests\Unit;

use App\Kuliner;
use App\Location;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Phaza\LaravelPostgis\Geometries\Point;
use Tests\TestCase;

class LocationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_can_morph_to_kuliner_instance()
    {
        $location = factory(Location::class)->create();

        $this->assertInstanceOf(Kuliner::class, $location->content);
    }

    /** @test */
    public function it_has_point_data_type()
    {
        $location = factory(Location::class)->create();

        $this->assertInstanceOf(Point::class, $location->point);
    }
}
