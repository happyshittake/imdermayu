<?php
namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Image;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ImageTest extends TestCase
{
    use DatabaseMigrations;

    /** @test **/
    public function it_has_unique_name()
    {
       $image = factory(Image::class)->create();
       
       $newImage = factory(Image::class)->create([
           'original_name' => $image->original_name
       ]);
        
       $this->assertNotEquals($image->name, $newImage->name);
    }
}
