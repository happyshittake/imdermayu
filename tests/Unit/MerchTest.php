<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Merch;
use Phaza\LaravelPostgis\Geometries\Point;
use App\User;
use App\Location;

class MerchTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $Merch = factory(Merch::class)->create([
            'title' => $name
        ]);

        $Merch2 = factory(Merch::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($Merch->slug, $Merch2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $Merch = factory(Merch::class)->create();

        $Merch->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $Merch->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $Merch = factory(Merch::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $Merch->author);

    }
}
