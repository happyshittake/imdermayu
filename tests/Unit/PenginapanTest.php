<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Penginapan;
use App\Location;
use App\User;
use Phaza\LaravelPostgis\Geometries\Point;

class PenginapanTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $penginapan = factory(Penginapan::class)->create([
            'title' => $name
        ]);

        $penginapan2 = factory(Penginapan::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($penginapan->slug, $penginapan2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $Penginapan = factory(Penginapan::class)->create();

        $Penginapan->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $Penginapan->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $Penginapan = factory(Penginapan::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $Penginapan->author);

    }
}
