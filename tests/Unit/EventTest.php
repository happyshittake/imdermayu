<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Event;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Location;
use App\User;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $Event = factory(Event::class)->create([
            'title' => $name
        ]);

        $Event2 = factory(Event::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($Event->slug, $Event2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $Event = factory(Event::class)->create();

        $Event->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $Event->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $Event = factory(Event::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $Event->author);

    }
}
