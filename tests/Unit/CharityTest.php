<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Charity;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Location;
use App\User;

class CharityTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $Charity = factory(Charity::class)->create([
            'title' => $name
        ]);

        $Charity2 = factory(Charity::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($Charity->slug, $Charity2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $Charity = factory(Charity::class)->create();

        $Charity->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $Charity->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $Charity = factory(Charity::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $Charity->author);

    }
}
