<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\OlehOleh;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Location;
use App\User;

class OlehOlehTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_has_unique_slug_attribute()
    {
        $name = 'tes aja';

        $olehOleh = factory(OlehOleh::class)->create([
            'title' => $name
        ]);

        $olehOleh2 = factory(OlehOleh::class)->create([
            'title' => $name
        ]);

        $this->assertNotEquals($olehOleh->slug, $olehOleh2->slug);
    }

    /** @test */
    public function it_has_a_location()
    {
        $olehOleh = factory(OlehOleh::class)->create();

        $olehOleh->location()->create([
            'point' => new Point(10, 10)
        ]);


        $this->assertInstanceOf(Location::class, $olehOleh->location);
    }

    /** @test */
    public function it_belongs_to_a_user()
    {
        $user = factory(User::class)->create();
        $OlehOleh = factory(OlehOleh::class)->create([
            'author_id' => $user->id
        ]);

        $this->assertInstanceOf(User::class, $OlehOleh->author);

    }
}
