<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Penginapan;
use Illuminate\Support\Carbon;
use App\User;

class PenginapanTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_penginapan_object()
    {
        $this->signIn();
        \factory(Penginapan::class)->times(30)->create();

        $response = $this->getJson(\route('penginapan.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Penginapan::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Penginapan::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('penginapan.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Penginapan::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Penginapan::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Penginapan::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('penginapan.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Penginapan::class)->create([
            'published_at' => null
        ]);

        \factory(Penginapan::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Penginapan::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('penginapan.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function delete_can_destroy_a_penginapan()
    {
        $this->signIn();
        $penginapan = \factory(Penginapan::class)->create();

        $this->deleteJson(\route('penginapan.delete', $penginapan->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('penginapans', ['id' => $penginapan->id]);
    }

    /** @test */
    public function a_user_can_create_new_penginapan()
    {
        $user = \factory(User::class)->create();

        $penginapan = \factory(Penginapan::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('penginapan.create'), [
            'title' => $penginapan->title,
            'body' => $penginapan->body,
            'image' => $penginapan->image,
            'published' => true,
            'contact' => $penginapan->contact,
            'published_at' => $penginapan->published_at->format('Y-m-d H:i:s'),
            'price' => $penginapan->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('penginapans', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_penginapan()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Penginapan::class)->create();

        $penginapan = \factory(Penginapan::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('penginapan.update', $target->id), [
            'title' => $penginapan->title,
            'body' => $penginapan->body,
            'image' => $penginapan->image,
            'ready_stock' => $penginapan->ready_stock,
            'contact' => $penginapan->contact,
            'published' => true,
            'published_at' => $penginapan->published_at->format('Y-m-d H:i:s'),
            'price' => $penginapan->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('penginapans', ['id' => $target->id, 'title' => $penginapan->title]);
    }
}
