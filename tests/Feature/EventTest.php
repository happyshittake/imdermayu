<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Event;
use App\User;
use Illuminate\Support\Carbon;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_event_object()
    {
        $this->signIn();
        \factory(Event::class)->times(30)->create();

        $response = $this->getJson(\route('event.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Event::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Event::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('event.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Event::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Event::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Event::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('event.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Event::class)->create([
            'published_at' => null
        ]);

        \factory(Event::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Event::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('event.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function delete_can_destroy_a_event()
    {
        $this->signIn();
        $event = \factory(Event::class)->create();

        $this->deleteJson(\route('event.delete', $event->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('events', ['id' => $event->id]);
    }

    /** @test */
    public function a_user_can_create_new_event()
    {
        $user = \factory(User::class)->create();

        $event = \factory(Event::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('event.create'), [
            'title' => $event->title,
            'body' => $event->body,
            'image' => $event->image,
            'published' => true,
            'published_at' => $event->published_at->format('Y-m-d H:i:s'),
            'start_time' => $event->start_time->format('Y-m-d H:i:s'),
            'price' => $event->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('events', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_event()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Event::class)->create();

        $event = \factory(Event::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('event.update', $target->id), [
            'title' => $event->title,
            'body' => $event->body,
            'image' => $event->image,
            'published' => true,
            'published_at' => $event->published_at->format('Y-m-d H:i:s'),
            'start_time' => $event->start_time->format('Y-m-d H:i:s'),
            'price' => $event->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('events', ['id' => $target->id, 'title' => $event->title]);
    }
}
