<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\User;

class ImageTest extends TestCase
{
    use DatabaseMigrations;


    /** @test */
    public function it_can_upload_images_file()
    {
        \Storage::fake();
        collect([
            'test1.png',
            'test2.jpeg',
            'test3.gif'
        ])->each(function ($name) {

            $response = $this->uploadFile($name);

            $data = $response->json()['data'];

            $this->assertEquals(config('app.url') . "/storage/{$name}", $data['url']);
            $this->assertDatabaseHas('images', [
                'original_name' => $name
            ]);
            \Storage::assertExists($name);
        });
    }

    protected function uploadFile($name)
    {
        $user = \factory(User::class)->create();
        $this->actingAs($user);

        $file = UploadedFile::fake()->image($name);

        return $this->postJson(route('media.store'), [
            'file' => $file,
            'caption' => 'tes caption'
        ]);
    }

    /** @test */
    public function it_can_generate_slugged_name_for_upload_file()
    {
        \Storage::fake('media');
        $response = $this->uploadFile('tes 2.jpeg');

        $url = $response->json()['data']['url'];
        $filename = array_slice(explode('/', rtrim($url, '/')), -1)[0];

        $this->assertEquals('tes-2.jpeg', $filename);
        $this->assertDatabaseHas('images', [
            'name' => 'tes-2'
        ]);
    }

    /** @test */
    public function it_can_delete_image()
    {
        \Storage::fake();
        $name = 'tes.jpeg';
        $this->uploadFile($name);

        $response = $this->deleteJson(route('media.delete', 'tes'));

        $response->assertStatus(200);
        \Storage::assertMissing($name);
        $this->assertDatabaseMissing('images', [
            'name' => $name
        ]);

    }
}
