<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_user_object()
    {
        $this->signIn();
        \factory(User::class)->times(30)->create();

        $response = $this->getJson(\route('user.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(User::class)->times(15)->create([
            'name' => 'unique-name'
        ]);

        \factory(User::class)->times(15)->create([
            'name' => 'different'
        ]);

        $response = $this->getJson(\route('user.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['name']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_active_statuses()
    {
        $this->signIn();
        \factory(User::class)->create([
            'name' => 'unactive user',
            'is_active' => false
        ]);

        \factory(User::class)->create([
            'name' => 'active user',
            'is_active' => true
        ]);

        $response = $this->getJson(\route('user.index', ['f' => 'active', 'q' => 'active']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('active user', $response['data'][0]['name']);
    }

    /** @test */
    public function index_can_filter_by_active_statuses()
    {

        \factory(User::class)->create([
            'is_active' => false
        ]);

        $user = \factory(User::class)->create([
            'is_active' => true
        ]);

        $this->actingAs($user);

        $filters = ['all', 'active', 'inactive'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('user.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(2, $response['data']);
            } else {
                $this->assertCount(1, $response['data']);
            }
        });
    }

    /** @test */
    public function an_admin_can_activate_user()
    {
        $this->signIn();
        $target = \factory(User::class)->create(['is_active' => false]);

        $this->postJson(\route('user.active', $target->id))
            ->assertStatus(200);

        $this->assertDatabaseHas('users', ['id' => $target->id, 'is_active' => true]);
    }

    /** @test */
    public function an_admin_can_deactivate_user()
    {
        $this->signIn();
        $target = \factory(User::class)->create(['is_active' => true]);

        $this->deleteJson(\route('user.inactive', $target->id))
            ->assertStatus(200);

        $this->assertDatabaseHas('users', ['id' => $target->id, 'is_active' => false]);
    }

    /** @test */
    public function an_admin_can_create_user()
    {
        $this->signIn();

        $data = $this->postJson(\route('user.create'), [
            'name' => 'tes name',
            'email' => 'name@name.com',
            'password' => '12345'
        ])->assertStatus(201)->json();

        $this->assertDatabaseHas('users', ['name' => $data['data']['name']]);
    }

    /** @test */
    public function an_admin_can_edit_user()
    {
        $this->signIn();

        $target = \factory(User::class)->create();

        $data = $this->patchJson(\route('user.update', $target->id), [
            'name' => 'blablabla',
            'password' => '12345'
        ])
            ->assertStatus(200)
            ->json();

        $this->assertDatabaseHas('users', ['id' => $target->id, 'name' => 'blablabla']);
    }
}
