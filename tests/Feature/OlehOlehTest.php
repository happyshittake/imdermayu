<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\OlehOleh;
use Illuminate\Support\Carbon;
use App\User;

class OlehOlehTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_oleholeh_object()
    {
        $this->signIn();
        \factory(OlehOleh::class)->times(30)->create();

        $response = $this->getJson(\route('oleholeh.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(OlehOleh::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(OlehOleh::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('oleholeh.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(OlehOleh::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(OlehOleh::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(OlehOleh::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('oleholeh.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(OlehOleh::class)->create([
            'published_at' => null
        ]);

        \factory(OlehOleh::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(OlehOleh::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('oleholeh.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function delete_can_destroy_a_oleholeh()
    {
        $this->signIn();
        $oleholeh = \factory(OlehOleh::class)->create();

        $this->deleteJson(\route('oleholeh.delete', $oleholeh->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('oleh_olehs', ['id' => $oleholeh->id]);
    }

    /** @test */
    public function a_user_can_create_new_oleholeh()
    {
        $user = \factory(User::class)->create();

        $oleholeh = \factory(OlehOleh::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('oleholeh.create'), [
            'title' => $oleholeh->title,
            'body' => $oleholeh->body,
            'image' => $oleholeh->image,
            'published' => true,
            'published_at' => $oleholeh->published_at->format('Y-m-d H:i:s'),
            'price' => $oleholeh->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('oleh_olehs', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_oleholeh()
    {
        $user = \factory(User::class)->create();

        $target = \factory(OlehOleh::class)->create();

        $oleholeh = \factory(OlehOleh::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('oleholeh.update', $target->id), [
            'title' => $oleholeh->title,
            'body' => $oleholeh->body,
            'image' => $oleholeh->image,
            'published' => true,
            'published_at' => $oleholeh->published_at->format('Y-m-d H:i:s'),
            'price' => $oleholeh->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('oleh_olehs', ['id' => $target->id, 'title' => $oleholeh->title]);
    }
}
