<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Kuliner;
use Illuminate\Support\Carbon;
use App\User;

class KulinerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_kuliner_object()
    {
        $this->signIn();
        \factory(Kuliner::class)->times(30)->create();

        $response = $this->getJson(\route('kuliner.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Kuliner::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Kuliner::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('kuliner.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Kuliner::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Kuliner::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Kuliner::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('kuliner.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function publish_kuliner_can_publish_a_kuliner()
    {
        $this->signIn();
        $kuliner = \factory(Kuliner::class)->create(['published_at' => null]);

        $this->postJson(\route('kuliner.publish', $kuliner->id))
            ->assertStatus(201);

        $refreshed = Kuliner::find($kuliner->id);

        $this->assertTrue($refreshed->published);
    }

    /** @test */
    public function a_user_can_create_new_kuliner()
    {
        $user = \factory(User::class)->create();

        $kuliner = \factory(Kuliner::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('kuliner.create'), [
            'title' => $kuliner->title,
            'body' => $kuliner->body,
            'image' => $kuliner->image,
            'price' => $kuliner->price,
            'open_time' => $kuliner->open_time,
            'close_time' => $kuliner->close_time,
            'published' => true,
            'published_at' => $kuliner->published_at->format('Y-m-d H:i:s'),
            'address' => $kuliner->address,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('kuliners', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_kuliner()
    {
        $user = \factory(User::class)->create();

        $targetKuliner = \factory(Kuliner::class)->create();

        $kuliner = \factory(Kuliner::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('kuliner.update', $targetKuliner->id), [
            'title' => $kuliner->title,
            'body' => $kuliner->body,
            'image' => $kuliner->image,
            'price' => $kuliner->price,
            'open_time' => $kuliner->open_time,
            'close_time' => $kuliner->close_time,
            'published' => true,
            'published_at' => $kuliner->published_at->format('Y-m-d H:i:s'),
            'address' => $kuliner->address,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('kuliners', ['id' => $targetKuliner->id, 'title' => $kuliner->title]);
    }

    /** @test */
    public function unpublish_kuliner_can_unpublish()
    {
        $this->signIn();
        $kuliner = \factory(Kuliner::class)->create();

        $this->deleteJson(\route('kuliner.unpublish', $kuliner->id))
            ->assertStatus(200);

        $refreshed = Kuliner::find($kuliner->id);

        $this->assertFalse($refreshed->published);
    }

    /** @test */
    public function delete_can_destroy_a_kuliner()
    {
        $this->signIn();
        $kuliner = \factory(Kuliner::class)->create();

        $this->deleteJson(\route('kuliner.delete', $kuliner->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('kuliners', ['id' => $kuliner->id]);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Kuliner::class)->create([
            'published_at' => null
        ]);

        \factory(Kuliner::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Kuliner::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('kuliner.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }
}
