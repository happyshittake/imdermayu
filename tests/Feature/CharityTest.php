<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Charity;
use Illuminate\Support\Carbon;
use App\User;

class CharityTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_charity_object()
    {
        $this->signIn();
        \factory(Charity::class)->times(30)->create();

        $response = $this->getJson(\route('charity.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Charity::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Charity::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('charity.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Charity::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Charity::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Charity::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('charity.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Charity::class)->create([
            'published_at' => null
        ]);

        \factory(Charity::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Charity::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('charity.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function delete_can_destroy_a_charity()
    {
        $this->signIn();
        $charity = \factory(Charity::class)->create();

        $this->deleteJson(\route('charity.delete', $charity->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('charities', ['id' => $charity->id]);
    }

    /** @test */
    public function a_user_can_create_new_charity()
    {
        $user = \factory(User::class)->create();

        $charity = \factory(Charity::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('charity.create'), [
            'title' => $charity->title,
            'body' => $charity->body,
            'image' => $charity->image,
            'published' => true,
            'address' => $charity->address,
            'contact' => $charity->contact,
            'bank_name' => $charity->bank_name,
            'bank_account_no' => $charity->bank_account_no,
            'bank_account_owner' => $charity->bank_account_owner,
            'published_at' => $charity->published_at->format('Y-m-d H:i:s'),
            'price' => $charity->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('charities', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_charity()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Charity::class)->create();

        $charity = \factory(Charity::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('charity.update', $target->id), [
            'title' => $charity->title,
            'body' => $charity->body,
            'image' => $charity->image,
            'address' => $charity->address,
            'bank_name' => $charity->bank_name,
            'bank_account_no' => $charity->bank_account_no,
            'bank_account_owner' => $charity->bank_account_owner,
            'contact' => $charity->contact,
            'published' => true,
            'published_at' => $charity->published_at->format('Y-m-d H:i:s'),
            'price' => $charity->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('charities', ['id' => $target->id, 'title' => $charity->title]);
    }
}
