<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Loker;
use Illuminate\Support\Carbon;
use App\User;

class LokerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_loker_object()
    {
        $this->signIn();
        \factory(Loker::class)->times(30)->create();

        $response = $this->getJson(\route('loker.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Loker::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Loker::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('loker.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Loker::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Loker::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Loker::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('loker.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Loker::class)->create([
            'published_at' => null
        ]);

        \factory(Loker::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Loker::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('loker.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function publish_loker_can_publish_a_loker()
    {
        $this->signIn();
        $loker = \factory(Loker::class)->create(['published_at' => null]);

        $this->postJson(\route('loker.publish', $loker->id))
            ->assertStatus(201);

        $refreshed = Loker::find($loker->id);

        $this->assertTrue($refreshed->published);
    }

    /** @test */
    public function unpublish_loker_can_unpublish()
    {
        $this->signIn();
        $loker = \factory(Loker::class)->create();

        $this->deleteJson(\route('loker.unpublish', $loker->id))
            ->assertStatus(200);

        $refreshed = Loker::find($loker->id);

        $this->assertFalse($refreshed->published);
    }

    /** @test */
    public function delete_can_destroy_a_loker()
    {
        $this->signIn();
        $loker = \factory(Loker::class)->create();

        $this->deleteJson(\route('loker.delete', $loker->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('lokers', ['id' => $loker->id]);
    }

    /** @test */
    public function a_user_can_create_new_loker()
    {
        $user = \factory(User::class)->create();

        $loker = \factory(Loker::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('loker.create'), [
            'title' => $loker->title,
            'body' => $loker->body,
            'image' => $loker->image,
            'published' => true,
            'published_at' => $loker->published_at->format('Y-m-d H:i:s'),
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('lokers', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_loker()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Loker::class)->create();

        $loker = \factory(Loker::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('loker.update', $target->id), [
            'title' => $loker->title,
            'body' => $loker->body,
            'image' => $loker->image,
            'published' => true,
            'published_at' => $loker->published_at->format('Y-m-d H:i:s'),
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('lokers', ['id' => $target->id, 'title' => $loker->title]);
    }
}
