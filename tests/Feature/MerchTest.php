<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Merch;
use Illuminate\Support\Carbon;
use App\User;

class MerchTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_merch_object()
    {
        $this->signIn();
        \factory(Merch::class)->times(30)->create();

        $response = $this->getJson(\route('merch.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Merch::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Merch::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('merch.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Merch::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Merch::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Merch::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('merch.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Merch::class)->create([
            'published_at' => null
        ]);

        \factory(Merch::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Merch::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('merch.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function index_can_filter_by_type()
    {
        $this->signIn();

        \factory(Merch::class)->create([
            'ready_stock' => false
        ]);

        \factory(Merch::class)->create([
            'ready_stock' => true
        ]);

        $filters = ['all', 'po', 'ready'];

        \collect($filters)->each(function ($type) {
            $response = $this->getJson(\route('merch.index', \compact('type')))
                ->json();

            if ($type == 'all') {
                $this->assertCount(2, $response['data']);
            }

            if ($type == 'po') {
                $this->assertCount(1, $response['data']);
            }

            if ($type == 'ready') {
                $this->assertCount(1, $response['data']);
            }
        });
    }

    /** @test */
    public function delete_can_destroy_a_merch()
    {
        $this->signIn();
        $merch = \factory(Merch::class)->create();

        $this->deleteJson(\route('merch.delete', $merch->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('merches', ['id' => $merch->id]);
    }

    /** @test */
    public function a_user_can_create_new_merch()
    {
        $user = \factory(User::class)->create();

        $merch = \factory(Merch::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('merch.create'), [
            'title' => $merch->title,
            'body' => $merch->body,
            'image' => $merch->image,
            'published' => true,
            'ready_stock' => $merch->ready_stock,
            'contact' => $merch->contact,
            'published_at' => $merch->published_at->format('Y-m-d H:i:s'),
            'price' => $merch->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('merches', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_merch()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Merch::class)->create();

        $merch = \factory(Merch::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('merch.update', $target->id), [
            'title' => $merch->title,
            'body' => $merch->body,
            'image' => $merch->image,
            'ready_stock' => $merch->ready_stock,
            'contact' => $merch->contact,
            'published' => true,
            'published_at' => $merch->published_at->format('Y-m-d H:i:s'),
            'price' => $merch->price,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('merches', ['id' => $target->id, 'title' => $merch->title]);
    }
}
