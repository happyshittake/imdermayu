<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Wisata;
use Carbon\Carbon;
use App\User;

class WisataTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function index_route_will_return_paginated_results_of_wisata_object()
    {
        $this->signIn();
        \factory(Wisata::class)->times(30)->create();

        $response = $this->getJson(\route('wisata.index'))
            ->assertStatus(200)
            ->json();

        $this->assertTrue(array_key_exists('data', $response));
        $this->assertTrue(array_key_exists('meta', $response));
    }

    /** @test */
    public function index_route_can_search_by_query()
    {
        $this->signIn();
        \factory(Wisata::class)->times(15)->create([
            'title' => 'its so unique'
        ]);

        \factory(Wisata::class)->times(15)->create([
            'title' => 'soo different'
        ]);

        $response = $this->getJson(\route('wisata.index', ['q' => 'unique']))
            ->assertStatus(200)
            ->json();

        \collect($response['data'])->each(function ($data) {
            $this->assertContains('unique', $data['title']);
        });
    }

    /** @test */
    public function index_can_search_and_filter_by_publishing_statuses()
    {
        $this->signIn();
        \factory(Wisata::class)->create([
            'title' => 'unpublished',
            'published_at' => null
        ]);

        \factory(Wisata::class)->create([
            'title' => 'published',
            'published_at' => Carbon::now()
        ]);

        \factory(Wisata::class)->create([
            'title' => 'delayed',
            'published_at' => Carbon::now()->addDay()
        ]);

        $response = $this->getJson(\route('wisata.index', ['f' => 'published', 'q' => 'published']))
            ->assertStatus(200)
            ->json();

        $this->assertEquals('published', $response['data'][0]['title']);
    }

    /** @test */
    public function index_can_filter_by_publishing_statuses()
    {
        $this->signIn();

        \factory(Wisata::class)->create([
            'published_at' => null
        ]);

        \factory(Wisata::class)->create([
            'published_at' => Carbon::now()
        ]);

        \factory(Wisata::class)->create([
            'published_at' => Carbon::now()->addDay()
        ]);

        $filters = ['all', 'published', 'unpublished'];

        \collect($filters)->each(function ($f) {
            $response = $this->getJson(\route('wisata.index', \compact('f')))
                ->json();

            if ($f == 'all') {
                $this->assertCount(3, $response['data']);
            }

            if ($f == 'published') {
                $this->assertCount(1, $response['data']);
            }

            if ($f == 'unpublished') {
                $this->assertCount(2, $response['data']);
            }
        });
    }

    /** @test */
    public function publish_wisata_can_publish_a_wisata()
    {
        $this->signIn();
        $wisata = \factory(Wisata::class)->create(['published_at' => null]);

        $this->postJson(\route('wisata.publish', $wisata->id))
            ->assertStatus(201);

        $refreshed = Wisata::find($wisata->id);

        $this->assertTrue($refreshed->published);
    }

    /** @test */
    public function unpublish_wisata_can_unpublish()
    {
        $this->signIn();
        $wisata = \factory(Wisata::class)->create();

        $this->deleteJson(\route('wisata.unpublish', $wisata->id))
            ->assertStatus(200);

        $refreshed = Wisata::find($wisata->id);

        $this->assertFalse($refreshed->published);
    }

    /** @test */
    public function delete_can_destroy_a_wisata()
    {
        $this->signIn();
        $wisata = \factory(Wisata::class)->create();

        $this->deleteJson(\route('wisata.delete', $wisata->id))
            ->assertStatus(200);

        $this->assertDatabaseMissing('wisatas', ['id' => $wisata->id]);
    }

    /** @test */
    public function a_user_can_create_new_wisata()
    {
        $user = \factory(User::class)->create();

        $wisata = \factory(Wisata::class)->make();

        $this->actingAs($user);

        $data = $this->postJson(\route('wisata.create'), [
            'title' => $wisata->title,
            'body' => $wisata->body,
            'image' => $wisata->image,
            'published' => true,
            'published_at' => $wisata->published_at->format('Y-m-d H:i:s'),
            'address' => $wisata->address,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(201)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('wisatas', ['id' => $data['data']['id']]);
    }

    /** @test */
    public function a_user_can_edit_a_wisata()
    {
        $user = \factory(User::class)->create();

        $target = \factory(Wisata::class)->create();

        $wisata = \factory(Wisata::class)->make();

        $this->actingAs($user);

        $data = $this->patchJson(\route('wisata.update', $target->id), [
            'title' => $wisata->title,
            'body' => $wisata->body,
            'image' => $wisata->image,
            'published' => true,
            'published_at' => $wisata->published_at->format('Y-m-d H:i:s'),
            'address' => $wisata->address,
            'lat' => 29.5,
            'lon' => 12.3
        ])
            ->assertStatus(200)
            ->json();

        $this->assertTrue(\array_key_exists('redirect', $data));
        $this->assertDatabaseHas('wisatas', ['id' => $target->id, 'title' => $wisata->title]);
    }
}
