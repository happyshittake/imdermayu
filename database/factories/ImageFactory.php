<?php

use Faker\Generator as Faker;
use App\Image;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Image::class, function (Faker $faker) {
    return [
        'original_name' => $faker->name,
        'caption' => $faker->sentence,
        'ext' => $faker->fileExtension
    ];
});
