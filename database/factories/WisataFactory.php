<?php
use Faker\Generator as Faker;
use App\Wisata;

$factory->define(Wisata::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'address' => $faker->address,
        'published_at' => $faker->dateTime,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
