<?php

use Faker\Generator as Faker;
use App\OlehOleh;

$factory->define(OlehOleh::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'published_at' => $faker->dateTime,
        'price' => $faker->randomDigitNotNull,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
