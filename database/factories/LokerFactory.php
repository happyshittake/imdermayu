<?php

use Faker\Generator as Faker;
use App\Loker;

$factory->define(Loker::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'published_at' => $faker->dateTime,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
