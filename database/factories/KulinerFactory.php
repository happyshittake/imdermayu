<?php
use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Kuliner::class, function (Faker $faker) {
    $openTime = $faker->time('H:i');
    $closeTime = \Carbon\Carbon::createFromFormat('H:i', $openTime)
        ->addHours($faker->numberBetween(0, 23))
        ->format('H:i');

    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'price' => $faker->numberBetween(1000, 3000000),
        'open_time' => $openTime,
        'close_time' => $closeTime,
        'address' => $faker->address,
        'published_at' => $faker->dateTime,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
