<?php

use Faker\Generator as Faker;
use App\Penginapan;

$factory->define(Penginapan::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'published_at' => $faker->dateTime,
        'price' => $faker->randomDigitNotNull,
        'contact' => $faker->phoneNumber,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
