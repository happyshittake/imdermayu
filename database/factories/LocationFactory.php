<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Location::class, function (Faker $faker) {
    $point = new \Phaza\LaravelPostgis\Geometries\Point(
        $faker->latitude, $faker->longitude
    );

    $kuliner = factory(\App\Kuliner::class)->create();

    return [
        'point' => $point,
        'location_id' => $kuliner->id,
        'location_type' => get_class($kuliner)
    ];
});
