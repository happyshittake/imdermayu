<?php

use Faker\Generator as Faker;
use App\Charity;

$factory->define(Charity::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'address' => $faker->address,
        'published_at' => $faker->dateTime,
        'bank_name' => $faker->company,
        'bank_account_no' => $faker->bankAccountNumber,
        'bank_account_owner' => $faker->name,
        'contact' => $faker->phoneNumber,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
