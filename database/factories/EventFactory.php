<?php

use Faker\Generator as Faker;
use App\Event;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'image' => $faker->imageUrl(),
        'start_time' => $faker->dateTime(),
        'published_at' => $faker->dateTime(),
        'price' => $faker->randomDigitNotNull,
        'author_id' => factory(\App\User::class)->create()->id
    ];
});
