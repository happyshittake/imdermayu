<?php
use Illuminate\Database\Seeder;
use App\Kuliner;
use App\Wisata;
use App\Loker;
use App\Event;
use App\OlehOleh;
use App\Penginapan;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kuliners = factory(Kuliner::class)->times(20)->create();
        $wisatas = factory(Wisata::class)->times(20)->create();
        $lokers = factory(Loker::class)->times(20)->create();
        $events = factory(Event::class)->times(20)->create();
        $Oleholehs = factory(OlehOleh::class)->times(20)->create(); 
        $penginapan = factory(Penginapan::class)->times(20)->create();       

        $cb = function ($k) {
            factory(\App\Location::class)->create(
                [
                    'location_id' => $k->id,
                    'location_type' => get_class($k)
                ]
            );
        };

        $kuliners->each($cb);
        $wisatas->each($cb);
        $lokers->each($cb);
        $events->each($cb);
        $Oleholehs->each($cb);
        $penginapan->each($cb);
    }
}
