<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKulinersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuliners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('body');
            $table->string('image');
            $table->decimal('price', 50, 0);
            $table->string('slug')->unique();
            $table->time('open_time');
            $table->time('close_time');
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->text('address')->nullable();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE kuliners ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX kuliners_searchable_index ON kuliners USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuliners');
    }
}
