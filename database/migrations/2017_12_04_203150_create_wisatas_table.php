<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWisatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wisatas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('image');
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->text('address')->nullable();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE wisatas ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX wisatas_searchable_index ON wisatas USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wisatas');
    }
}
