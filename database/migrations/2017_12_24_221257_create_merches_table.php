<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('image');
            $table->decimal('price', 50, 0);
            $table->boolean('ready_stock');
            $table->string('contact');
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE merches ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX merches_searchable_index ON merches USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merches');
    }
}
