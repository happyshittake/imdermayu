<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenginapansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penginapans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('image');
            $table->decimal('price', 50, 0);
            $table->string('contact');
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE penginapans ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX penginapans_searchable_index ON penginapans USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penginapans');
    }
}
