<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->text('address');
            $table->string('image');
            $table->string('contact');
            $table->string('bank_name');
            $table->string('bank_account_no');
            $table->string('bank_account_owner');
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE charities ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX charities_searchable_index ON charities USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charities');
    }
}
