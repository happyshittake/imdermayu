<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('image');
            $table->timestamp('start_time')->nullable();
            $table->decimal('price', 50, 0);
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE events ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX events_searchable_index ON events USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
