<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlehOlehsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oleh_olehs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->string('image');
            $table->decimal('price', 50, 0);
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('author_id');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE oleh_olehs ADD searchable tsvector NULL');
        DB::statement('CREATE INDEX oleh_olehs_searchable_index ON oleh_olehs USING GIN (searchable)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oleh_olehs');
    }
}
